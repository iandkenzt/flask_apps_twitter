# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### Installation ###

* Setup App & Environment:
	1) clone repo
	3) create virtual environment: virtualenv -p python3.5 twitter --no-site-packages

* Run App
	1) activate virtualenv: source your/path/environment/bin/activate
	2) install requirements: pip3.5 install -r requirements.txt (position in directory app)
	3) move to folder App: python3.5 main.py
