import os
from operator import itemgetter

from datetime import datetime
from flask import Flask, render_template, request, redirect, url_for, session
from flask_pymongo import PyMongo
from flask_wtf.csrf import CSRFProtect
from simplecrypt import encrypt, decrypt
from werkzeug.utils import secure_filename


UPLOAD_FOLDER = os.path.join(os.getcwd(), 'static/img')
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])


app = Flask(__name__)
app.config['MONGO_DBNAME'] = 'twitter'
app.config['MONGO_URI'] = 'mongodb://localhost:27017/twitter'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['STATIC_FOLDER'] = 'static'


csrf = CSRFProtect()
csrf.init_app(app)


mongo = PyMongo(app)


@app.route('/')
def index():

    if 'username' in session:
        get_status = mongo.db.status.find()

        list_status = [i for i in get_status]

        for i in list_status:
            get_user = mongo.db.user.find_one({'_id':i['user_id']})
            i['user'] = get_user

        list_status.sort(key=itemgetter('created_at'), reverse=True)

        user = mongo.db.user.find_one({'email':session['username']})

        return render_template('index.html', list_status=list_status,\
                                user_id = user['_id'])

    return render_template('login.html')


@app.route('/login', methods=['POST', 'GET'])
def login():

    if request.method == 'POST':
        email = request.form['email']
        password = request.form['password']

        # check request POST
        if email == '' and password == '':
            status_log = "Form tidak lengkap"
            return render_template('login.html', status_log=status_log)

        # check user to db
        user = mongo.db.user
        login_users = user.find_one({'email':email})

        if login_users:
            status_log = "User tidak terdaftar, silahkan registrasi."

            decrypt_pwd = decrypt(email.encode('utf8'), login_users['password'])

            if decrypt_pwd.decode("utf-8") == password:
                session['username'] = email
                session['logged_in'] = True
                return redirect(url_for('index'))

        return render_template('login.html', status_log='Invalid email/password')

    if request.method == 'GET':
        return redirect(url_for('index'))


@app.route('/logout', methods=['POST', 'GET'])
def logout():

    session.pop('username', None)
    session.pop('logged_in', None)
    return redirect(url_for('index'))


@app.route('/register', methods=['POST', 'GET'])
def register():

    if request.method == 'POST':
        email = request.form['email']
        name = request.form['name']
        password = request.form['password']

        # check request POST
        if email == '' or name == '' or password== '':
            status_reg = "Email/name/password not Null"
            return render_template('login.html', status_reg=status_reg)

        user = mongo.db.user

        exsist_user = user.find_one({'email':email})

        if exsist_user is None:
            # Insert data
            crypt_pwd = encrypt(email.encode('utf8'), password.encode('utf8'))
            new_user = {"name" : name, "password" : crypt_pwd, \
                        "email" : email, "path_image":""}
            user.insert(new_user)

            # Auto login
            session['username'] = email
            session['logged_in'] = True
            return redirect(url_for('index'))
        else:
            status_reg = "User already."
            return render_template('login.html', status_reg=status_reg)

    if request.method == 'GET':
        return redirect(url_for('index'))


@app.route('/profile', methods=['POST', 'GET'])
def profile():
    if 'username' in session:

        con = mongo.db.user
        user = con.find_one({'email':session['username']})

        context = {}
        context['email'] = user['email']
        context['name'] = user['name']
        context['path_image'] = user['path_image']

        if request.method == 'POST':
            email = request.form['email']
            email_hide = request.form['email_hide']
            name = request.form['name']
            password = request.form['password']

            if email != email_hide:
                exsist_user = mongo.db.user.find_one({'email':email})

                if exsist_user is not None:
                    context['status'] = "Email User Already"
                    return render_template('profile.html', user=context)


            if email == '':
                context['status'] = 'Email not Null'
                return render_template('profile.html', user=context)

            if password == '':
                con.update_one(
                    {'email':session['username']},
                    {'$set':{
                        'email':email,
                        'name':name,
                        }
                    }
                )
            else:
                password_up = encrypt(email.encode('utf8'), password.encode('utf8'))

                con.update_one(
                    {'email':session['username']},
                    {'$set':{
                        'email':email,
                        'name':name,
                        'password':password_up
                        }
                    }
                )

            # update session
            session.pop('username', None)
            session['username'] = email

            context = {}
            context['email'] = email
            context['name'] = name
            context['password'] = password
            context['path_image'] = user['path_image']
            context['status'] = "Update success"

            return render_template('profile.html', user=context)

        if request.method == 'GET':
            con = mongo.db.user
            user = con.find_one({'email':session['username']})

            context = {}
            context['email'] = user['email']
            context['name'] = user['name']
            context['password'] = user['password']
            context['path_image'] = user['path_image']

            return render_template('profile.html', user=context)

    # user not login and will direct to root url
    return redirect(url_for('index'))


@app.route('/upload', methods=['POST', 'GET'])
def upload():

    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            return redirect(request.url)

        files = request.files['file']
        # if user does not select file, browser also
        # submit a empty part without filename
        if files.filename == '':
            return redirect(request.url)

        print(files.filename)

        if files and allowed_file(files.filename):
            con = mongo.db.user
            user = con.find_one({'email':session['username']})

            if user is not None:
                filename = secure_filename(files.filename)
                path_filename = os.path.join(app.config['UPLOAD_FOLDER'], filename)
                files.save(path_filename)

                path_db = os.path.join('/static/img', filename)

                con.update_one(
                    {'email':session['username']},
                    {
                        '$set':{'path_image':path_db}
                    }
                )

            return redirect(url_for('profile'))

    return redirect(url_for('index'))

@app.route('/uploads')
def uploaded_file(filename):
    print('ok')
    return send_from_directory(app.config['UPLOAD_FOLDER'],
                               filename)

@app.route('/update_status', methods=['POST', 'GET'])
def update_status():

    if 'username' in session:
        if request.method == 'POST':
            status = request.form['status']

            date_status = datetime.now()

            user = mongo.db.user.find_one({'email':session['username']})

            store_status = {'status':status, 'created_at':date_status, \
                            'updated_at':date_status, 'user_id':user['_id']}

            con = mongo.db.status
            con.insert(store_status)

            return redirect(url_for('index'))


def allowed_file(filename):
    return '.' in filename and \
                filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


if __name__ == '__main__':
    app.secret_key = 'twitter_app'
    app.run(debug=True)
